fun main(args:Array<String>){
    val rect = Rectangle(1, 2)
    println("area is" +rect.area())
    val rational1 = Rational(1, 2)
    val rational2 = Rational(1, 2)
    println(rational1.rationalAddition(rational1, rational2))
    println(rational1.rationalEquals(rational1,rational2))
    println(rational1.rationalCompareto(rational1,rational2))
    println(rational1.equal(rational1,rational2))
    println(rational1.less(rational1,rational2))
    println(squareAll(arrayOf(1, 2, 4, 5, 6)))
    println(filterAllEven(arrayOf(1, 2, 3, 4, 6, 10)))
    println(minmax(arrayOf(10, 71, 2, 3, 94, 57, 60)))
    println(concatTwoArray(arrayOf(1, 2, 4, 5), arrayOf(6, 7, 8, 9)))
    println(isAllEven(arrayOf(2, 4, 6, 8, 12)))
    println(isAllEven(arrayOf(1, 3, 7, 9, 11)))
    println(isAllEven(arrayOf(2, 3, 7, 9, 16)))
    println(isAllEven(arrayOf(21, 31, 72, 96, 16)))
         }


fun max(x: Int, y: Int) = if(x > y) x else y


fun max(x: Int, y: Int, z: Int) = max(max(x,y),z)


fun lcm(x: Int, y: Int, z: Int): Int {
    var m = max(x, y, z)
   while (true) {
        if (m % x == 0 && m % y == 0 && m % z == 0)
            return m

        m++
    }
}

fun min(x: Int, y: Int): Int = if (x < y) x else y

fun min(x: Int, y: Int, z: Int): Int = min(min(x, y),z)



fun gcd(x: Int, y: Int, z: Int): Int {
    var minimum = min(x, y, z)
    while(true){
        if(x % minimum == 0 && y % minimum == 0 && z % minimum == 0)
            return minimum
        minimum--
    }

}


fun isLeap(year: Int): Boolean = year % 400 == 0 || year % 4 == 0 && year % 100 != 0

/fun daysInMonth(month: Int , year: Int) =
       when(month) {
           2 -> if (isLeap(year)) 29 else 28
           4, 6, 9, 11 -> 30
           else -> 31
    }
fun isMonthValid(month: Int) = month in 1..12

fun isYearValid(year: Int) = year >= 1582


fun isDayValid(date: Int, month: Int, year: Int) = date in 1..daysInMonth(month, year)


fun isDateValid(date: Int, month: Int, year: Int) = isYearValid(year) && isMonthValid(month) && isDayValid(date, month, year)


fun reverseNum(n: Int): Int {
    var sum = 0
    var num = n
    while(num != 0){
        var n = num % 10
        sum = sum  * 10 + n
        num /= 10
    }
    return sum
}

fun factorial(n: Int): Int {
    var fact =1
    for(i in 1..n){
        fact *= i
    }
    return fact
}


fun ncr(n: Int, r: Int): Int {
    return factorial(n) / (factorial(r) * factorial(n - r))
}

fun isPerfect(n: Int): Boolean {
    var sum = 0
    for (i in 1..n) {
        if (n % i == 0)
            sum += i
    }
    return sum == n
    }

fun power(m: Int, n: Int): Int {
    var result = 1
    for (i in 1..n) {
        result *= m
    }
    return result
}

fun individualCount(n: Int): Int {
    var count = 0
    var num = n
    while(num != 0){
        var n = num % 10
        count += 1
        num /= 10
    }
    return count
}

fun isArmstrong(n: Int): Boolean {
   var sum = 0
    var num = n
    while(num != 0){
        var count = individualCount(n)
        var n = num % 10
        sum += power(n, count)
        num /= 10
    }
    return sum == n
}
fun shuffleArray(length: Int): List<Int> {
var arr = mutableListOf<Int>()
    var r = Random()
    while(arr.size < length) {
        var x = r.nextInt(length)
        if (!arr.contains(x))
            arr.add(x)

    }
    return arr
}


fun isSortedArray(arr:Array<Int>): Boolean {
    var index = 0
    while(index < arr.size - 1) {
       if (arr[index] > arr[index + 1]) {
            return false
        }
        index++
    }
    return true
}


fun uniqueElements(arr: Array<Int>): MutableList<Int> {
    var result = mutableListOf<Int>()
    for (index in 0..arr.size - 1) {
        if (!result.contains(arr[index])) {
            result.add(arr[index])
        }
    }
    return result
}

fun reverseArray(arr: ArrayList<Int>): List<Int> {
    var start = 0
    var end = arr.size - 1
    while (start < end) {
        var temp = arr[start]
        arr[start] = arr[end]
        arr[end] = temp
        start++
        end--
    }
    return arr
}

fun isUnique(arr: Array<Int>): Boolean {
    var index = 0
    while(index < arr.size -1) {
        for(j in index + 1..arr.size - 1 ) {
            if (arr[index] == arr[j])
                return false
        }
        index++
    }
    return true
}

fun uniqueelements(arr: Array<Int>): List<Int> {
    var result = mutableListOf<Int>()
    result.add(arr[0])
    var index = 1
    while(index <= arr.size -1) {
        var b = result.size -1
        if(result[b] != arr[index])
            result.add(arr[index]
        index++
    }
    return result
}


fun pascalLine(n: Int): List<Int> {
    var result = mutableListOf<Int>()
    for(i in 0..n)
        result.add(ncr(n, i))
    return result
}

fun pascalTriangle(n: Int): List<List<Int>> {
    var result = mutableListOf<List<Int>>()
    for(i in 0..n) {
        result.add(pascalLine(i))
    }
    return result
}

fun fisheryateshuffleArray(arr: MutableList<Int>): List<Int> {
    var r = Random()
    for(i in 0..arr.size -1) {
        var x = r.nextInt(arr.size)
        var temp = arr[i]
        arr[i] = arr[x]
        arr[x] = temp
    }
    return arr
}

fun matrixAddition(arr1: Array<Int>, arr2: Array<Int>):List<Int> {
    var result = mutableListOf<Int>()
    for (i in 0..arr1.size - 1) {
        for (j in 0..arr2.size - 1) {
            if(i == j) {
                result.add(arr1[i] + arr2[j])
            }

        }

    }
   return result
}


fun slice(arr: List<Int>, start: Int, end: Int): List<Int> {
    var result = mutableListOf<Int>()
    for(i in start until end) {
        result.add(arr[i])
    }
    return result
}


fun into2DArray(arr: List<Int>,col: Int): MutableList<List<Int>> {
    var result = mutableListOf<List<Int>>()
    val rows = arr.size / col
    var i = 0
    while(i < arr.size - 1 ) {
        var start = i * col
        var stop = i * col + col
        if(stop <= arr.size)
        result.add(slice(arr, start, stop))
        i++
    }
    return result
}

class Rectangle(val width: Int, val height: Int) {
    fun area(): Int = width * height
    fun perimeter(): Int = 2 * (height + width)
    }


fun squareAll(arr: Array<Int>): List<Int> {
    var result = mutableListOf<Int>()
    for(i in 0 until arr.size)
        result.add(arr[i] * arr[i])
    return result
}

fun isAllEven(arr: Array<Int>): Boolean {
    var i = 0
    while(i < arr.size) {
        if(arr[i] %  2 != 0) {
            return false
        }
        i++
    }
    return true
}


fun filterAllEven(arr: Array<Int>): List<Int> {
    var result = mutableListOf<Int>()
    for(i in 0 until arr.size) {
        if(arr[i] % 2 == 0) {
            result.add(arr[i])
        }
    }
    return result
}


fun minmax(arr: Array<Int>): Pair<Int, Int> {
   var max = arr[0]
   var min = arr[0]
    for(i in 1 until arr.size) {
        if(arr[i] > max) {
            max = arr[i]
        }
        if(arr[i] < min) {
            min = arr[i]
        }
    }
    return Pair(min, max)
}


fun concatTwoArray(arr1: Array<Int>, arr2: Array<Int>): MutableList<Int> {
    var result = mutableListOf<Int>()
    for(i in 0 until arr1.size) result.add(arr1[i])
    for(i in 0 until arr2.size) result.add(arr2[i])
    return result
}

//fun concatnArray(arr: ArrayList<Array<Int>>): MutableList<Int> {
//    var result = mutableListOf<Int>()
//    for(i in 0 until arr.size) {
//        result = concatTwoArray(result, arr[i])
//    }
//    return result
//}

class Rational (val numerator: Int, val denominator: Int) {
    fun rationalAddition(r1: Rational, r2: Rational): Rational {
        val n = r1 .numerator * r2.denominator + r2.numerator * r1.denominator
        val d = r1.denominator * r2.denominator
        return Rational(n,d)
    }

    fun rationalSubtraction(r1: Rational, r2: Rational): Rational {
        val n = r1 .numerator * r2.denominator - r2.numerator * r1.denominator
        val d = r1.denominator * r2.denominator
        return Rational(n,d)
    }

    fun rationalMultiplication(r1: Rational, r2: Rational): Rational {
        val n = r1 .numerator * r2.numerator
        val d = r1.denominator * r2.denominator
        return Rational(n,d)
    }

    fun rationalDivision(r1: Rational, r2: Rational): Rational {
        val n =  r1 .numerator * r2.denominator
        val d =  r2.numerator * r1.denominator
        return Rational(n,d)
    }

    fun rationalEquals(r1: Rational, r2: Rational): Boolean {
        return r1.numerator == r2.numerator && r1.denominator == r2.denominator
    }

    fun rationalCompareto(r1: Rational, r2: Rational): Int {
        return r1.numerator * r2.denominator - r1.denominator * r2.numerator
    }

    fun less(r1: Rational, r2: Rational): Boolean {
        return rationalCompareto(r1, r2 ) < 0
    }

    fun equal(r1: Rational, r2: Rational): Boolean {
        return rationalCompareto(r1, r2) == 0
    }
}
